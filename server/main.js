/**
 * Created by Group 3 on 19 Oct 2016
 */
const express = require("express");
const app = express();

const path = require("path");
const docroot = path.join(__dirname, "..");

app.set("port", process.argv[2] || process.env.NODE_PORT || 3000);
app.use(express.static(path.join(docroot, "client")));
app.use("/bower_components",
    express.static(path.join(docroot, "bower_components")));

app.listen(app.get("port"), function () {
    console.log("Server running at http://localhost: %s", app.get("port"));
});



// parse body of req
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.post("/api/users", function (req, res) {
    console.log(req.body);
    console.log(req.query);
    console.log(req.params);
    if(req.body.reply == 1){
        res.json(req.body);
    }
    else {
        res.status(500).json(req.body);
    }
});



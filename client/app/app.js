/**
 * Created by Group 3 on 19 Oct
 */
(function(){
    var MyApp = angular.module("MyApp", []);


    /* Version 1 */
    var MyCtrl = function ($http) {

        var myCtrl = this;

        var UserRegForm = {
            Email:"",
            Password:"",
            Name: "",
            Gender:"",
            DateofBirth: "",
            Address: "",
            Country: "",
            ContactNo:""
        }

        myCtrl.show = true;
        myCtrl.UserRegForm = angular.copy(UserRegForm);
        myCtrl.UserRegForm = [];
        myCtrl.server_response = {};


        myCtrl.message = "Thank You";
        myCtrl.submit = function submit(){
            console.log(myCtrl.UserRegForm);

            $http
                .post("/api/users",myCtrl.UserRegForm)
                //.post("/api/users/123456789?test=true&mock=true",{firstName: "Jon", lastName: "Snow"})
                .then(function (response) {
                    // Display message on the same page
                    myCtrl.server_response = response.data;
                    console.log("Thank you", myCtrl.server_response);
                })
                .catch(function (response) {
                    // Display message on the same page
                    myCtrl.server_response = response.data;
                    console.log("Thank You", myCtrl.server_response);
                });

            myCtrl.UserRegForm = angular.copy(UserRegForm);
        }

        //----------------

        console.log("The controller starts...");
    };

    // Associate the Angular Controller to the function?
    MyApp.controller("MyCtrl", [ "$http", MyCtrl ]);


})();